import axios from 'axios';
import offlineUserList from "../Assets/randomPeople";

const userGetConnector = axios.create({
    method: 'get'
});

export const getUser = (number) => {
    //FIXME randomuser is down, use again when is up again, don't forget to rechange name of variable in realisation3.js
    /*let url = "https://randomuser.me/api/";
    url = url.concat("?results=", number);

    if(number && number > 0){
        url = url.concat("?results=", number);
    }

    return userGetConnector.get(url)
        .then(response => {
            return response;
        }).catch(err => console.log(err));*/
    //FIX to use offline dataset from uinames (use url.concat("?amount=", number, '&ext'))
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve({data: offlineUserList.slice(0, number-1)});
        }, 300);
    });
};

export const getListAsso = () => {
    let url = "http://413fa2f2.ngrok.io/api/v1/directory/80/";

    return userGetConnector.get(url)
        .then(response => {
            return response;
        }).catch(err => console.log(err));
};