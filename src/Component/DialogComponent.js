import React from 'react';
import * as PropTypes from "prop-types";

import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from '@material-ui/core';

DialogComponent.propTypes = {
    open: PropTypes.bool.isRequired,
    closingHandler: PropTypes.func.isRequired,
    breakPointWidth: PropTypes.string.isRequired,
    fullWidth: PropTypes.bool.isRequired,
    overrideContent: PropTypes.bool,
};

export default function DialogComponent({person, open, closingHandler, breakPointWidth, fullWidth, overrideContent, ...props}) {
    return (
        <Dialog aria-labelledby="Dialog-Content-title" fullWidth={fullWidth} maxWidth={breakPointWidth}
                onClose={closingHandler} open={open} scroll={'paper'}>
            <DialogTitle id="Dialog-Content-title">{person.name + " " + person.surname}</DialogTitle>
            {(overrideContent !== undefined) && (overrideContent === true)
                ? props.children
                : <React.Fragment>
                    <DialogContent dividers={true}>
                        <p>First : {person.name}</p>
                        <p>Last : {person.surname}</p>
                        <p>Mail : {person.email}</p>
                        <p>Gender : {person.gender}</p>
                        <p>Phone : {person.phone}</p>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={closingHandler} color="primary">Close</Button>
                    </DialogActions>
                  </React.Fragment>
            }

        </Dialog>
    );
}
