import React, { Component } from 'react';
import {Typography} from "@material-ui/core";
import * as PropTypes from "prop-types";


class Exemple_classe extends Component {
    constructor(props) {
        super(props);
        this.state = {content: props.text};
    };

    render() {
        return (
            <React.Fragment>
                <Typography component={"p"}>Je suis une classe</Typography>
                <Typography component={"p"}>{this.state && this.state.content
                                                ? this.state.content
                                                : "Mais je n'ai rien à afficher"}
                </Typography>
            </React.Fragment>
        );
    };
}

Exemple_classe.propTypes = {
    text: PropTypes.string.isRequired
};
export default Exemple_classe;