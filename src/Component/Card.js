import React from 'react';
import * as PropTypes from "prop-types";

import {Button, Card, CardActions, CardContent,
    CardMedia, Typography, createStyles, makeStyles } from '@material-ui/core';
import Avatar from "@material-ui/core/Avatar";

CardComponent.propTypes = {
    address: PropTypes.string.isRequired,
    clickHandler: PropTypes.func.isRequired,
    img: PropTypes.string.isRequired,
    imgAlt: PropTypes.string.isRequired,
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

const useStyles = makeStyles(
    createStyles(theme => ({
            card: {
                maxWidth: '20vw',
                minHeight: '50vh',
                minWidth: '4vw'
            },
            img_card: {
                padding: '2.5vw',
                minHeight: '2.5vw',
                minWidth: '2.5vw'
            },
            button: {
                '&:hover': {
                    color: theme.palette.secondary.dark,
                },
            },
            area: {
                minHeight: '48vh'
            },
            bigAvatar: {
                margin: 10,
                width: 60,
                height: 60,
            },
        }),
    )
);

function CardComponent(props) {
    const classes = useStyles();
    const openDetail = (event) => {
        props.clickHandler(props.personIndex);
    };

    return (
        <Card className={classes.card}>
            <CardMedia alt={props.imgAlt} className={classes.bigAvatar} component={Avatar}
                       image={props.img} max-height="70" title={props.title}/>
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                    {props.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                    {props.address}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" component={"p"} className={classes.button} onClick={openDetail}>
                    See more
                </Button>
            </CardActions>
        </Card>
    );
}

export default CardComponent;
