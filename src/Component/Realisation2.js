import React from 'react';
import {Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

export function Realisation2(){
    const [time, setTime] = React.useState(0);
    const [timerId, setTimerId] = React.useState(null);
    const [isPair, setIsPair] = React.useState(true);

    const handleClickStart = (event) => {
        clearInterval(timerId);
        const timerTmp = setInterval(() => {
            setTime(prevTime => prevTime + 1);
        }, 100);
        setTimerId(timerTmp);
    };

    const handleClickReset = (event) => {
        setTime(0);
        setIsPair(true);
        clearInterval(timerId);
    };

    React.useEffect(() => {
        if(Math.floor(time/60) % 2 !== 0){
            setIsPair(true);
        }else{
            setIsPair(false);
        }
    },[time]);

    return(
        <React.Fragment>
            <Grid container direction="row" justify="center" alignItems="center">
                <Grid item md={2}/>
                <Grid item md={4}>
                    <Typography component={"h2"} style={{color: isPair ? "black" : "red"}}>{time}</Typography>
                </Grid>
                <Grid item md={2}>
                    <Button variant="contained" color="primary" onClick={handleClickStart}>Start</Button>
                </Grid>
                <Grid item md={2}>
                    <Button variant="contained" color="primary" onClick={handleClickReset}>Reset</Button>
                </Grid>
                <Grid item md={2}/>
            </Grid>
        </React.Fragment>
    );
}