import React from 'react';
import * as PropTypes from "prop-types";
import {TextField, Typography} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";

Realisation1.propTypes = {
    text: PropTypes.string.isRequired
};

export function Realisation1({text}){
    const [content, setContent] = React.useState(text);
    const [fieldValue, setFieldValue] = React.useState("");

    const handleClick = (event) => {
        setContent(fieldValue);
    };

    const handleChange = (event) => {
        setFieldValue(event.target.value);
    };

    return(
        <React.Fragment>
            <Grid container direction="row" justify="center" alignItems="center">
                <Grid item md={2}/>
                <Grid item md={3}>
                    <TextField id="standard-name" label="Text to replace" value={fieldValue} onChange={handleChange}/>
                </Grid>
                <Grid item md={1}>
                    <Button variant="contained" color="primary" onClick={handleClick}>Remplacer</Button>
                </Grid>
                <Grid item md={2}/>
                <Grid item md={3}>
                    <Typography component={"p"}>{content}</Typography>
                </Grid>
                <Grid item md={1}/>
            </Grid>
        </React.Fragment>
    );
}